@extends('layouts.web')
@section('content')

{{-- Scroll Button --}}

<button id="myBtn" title="Go to top"><i class="fas fa-angle-up "></i></button>

<!-- Carousel Slide -->

@include('web.home.slideshow')

<!-- Carousel Discount -->

@include('web.home.discount')

<!-- Section Top Products -->

@include('web.home.products')

<!-- Section Multi Image -->

@include('web.home.images')

<!-- Section News -->

@include('web.home.news')

@push('js')
    <script>
        $(function() {
            var btn = $('#myBtn');

            $(window).scroll(function() {
                if ($(window).scrollTop() < 300) {
                btn.addClass('d-none');
                } else {
                btn.removeClass('d-none');
                }
            });

            btn.on('click', function(e) {
                e.preventDefault();
                $('html, body').animate({scrollTop:0}, 600);
            });

        });
    </script>
@endpush

@endsection
