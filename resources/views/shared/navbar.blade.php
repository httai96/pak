<nav class="navbar navbar-dark navbar-expand-lg bg-primary navbar-top sticky-top border-bottom">
    <div class="container">
        <button class="navbar-toggler mr-4" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" 
                aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="mr-auto d-none d-lg-block">
            <small class="welcome-text" id="welcomeText">
                Chào mừng quý khách đến với Phúc An Khang Electronic
            </small>
            <a href="#" class="mx-auto logo-nav-scroll d-none" id="logoNavScroll">
                <img src="{{ asset('img/Group1.png') }}" alt="" style="height: 18px;">
            </a>
        </div>
         
        <div class="d-none d-lg-block">
            <a href="#" class="language-nav">
                <small class="language">VN&nbsp;<i class="fas fa-angle-down"></i></small>
            </a>
        </div>
        
        <div class="pl-2 d-none d-lg-block">
            <a href="#" class="login-nav">
                <small class="login">Đăng Nhập</small>
            </a>
        </div>
        <a href="#" class="mr-auto d-block d-lg-none logo-nav">
            <img src="{{ asset('img/Group1.png') }}" alt="">
        </a> 
        <div class="search-button d-lg-none mr-4">
            <a class="search-button__left" data-toggle="collapse" href="#collapseExample" 
            role="button" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-search"></i>
            </a>
        </div>
        
        <div class="nav-item cart-button d-lg-none">
            <a class="shopping-cart-nav" href="#">
                <i class="fas fa-shopping-cart"></i>
                <span class="badge badge-pill badge-warning notify-badge">9+</span>
            </a>
        </div>
    </div>
</nav>
<div class="collapse search-collapse " id="collapseExample">
    <div class="card card-body d-block d-lg-none">
        <form action="" method="post">
            @csrf
            <div class="input-group d-flex">
                <div class="input-group-prepend" style="width: 50%;">
                    <select class="form-control" name="#" id="#">
                        <option value="" selected disabled hidden> Danh mục</option>
                        <option value="1">Một</option>
                        <option value="2">Hai</option>
                        <option value="3">Ba</option>
                    </select>
                </div>
                <input type="text" class="form-control search-input flex-grow-1" 
                name="" id="" placeholder="Nhập từ khóa" style="width: 0;">
            </div>
        </form>
    </div>
</div>
<div class="collapse navbar-collapse text-center bg-primary d-lg-none" id="navbarSupportedContent2">
    <ul class="navbar-nav d-lg-none">
        <li class="nav-item">
            <a class="nav-link pl-0 font-weight-bold text-uppercase" href="{{ url('/') }}">Trang chủ</a>
        </li>
        <li class="nav-item">
            <a class="nav-link font-weight-bold text-uppercase" href="content">Giới thiệu</a>
        </li>
        <li class="nav-item">
            <a class="nav-link font-weight-bold dropdown-toggle text-uppercase" href="content">Sản Phẩm</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link font-weight-bold text-uppercase" href="category">khuyến mãi</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link font-weight-bold text-uppercase" href="category">video</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link font-weight-bold text-uppercase" href="category">liên hệ</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link font-weight-bold text-uppercase" href="category">
                VN
                <i class="fas fa-angle-down"></i>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link font-weight-bold text-uppercase" href="category">
                Đăng Nhập
            </a>
        </li>
    </ul>

</div>
<div class="logo-mid text-center py-3 d-none d-lg-block bg-primary">
    <div class="container">
        <img href="#" src="{{ asset('img/Group1.png') }}">
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary d-none d-lg-block text-center py-2 navbar-bot">
    <div class="container">
        {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button> --}}
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto d-flex">
                <li class="nav-item">
                    <a class="nav-link pl-0 font-weight-bold text-uppercase" href="{{ url('/') }}">Trang chủ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold text-uppercase" href="content">Giới thiệu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold dropdown-toggle text-uppercase" href="content">Sản Phẩm</a>
                </li>
                <li class="nav-item mx-auto input-group-main d-none d-lg-block">
                    <form action="" method="post">
                        @csrf
                        <div class="input-group d-flex">
                            <div class="input-group-prepend">
                                <select class="form-control " name="#" id="#">
                                    <option value="" selected disabled hidden> Danh mục</option>
                                    <option value="1">Một</option>
                                    <option value="2">Hai</option>
                                    <option value="3">Ba</option>
                                </select>
                            </div>
                            <input type="text" class="form-control search-input flex-grow-1" name="" id="" placeholder="Tìm kiếm">
                            <button type="submit" class="btn border-0 btn-search"><i class="fas fa-search"></i></button>                   
                        </div>
                    </form>
                </li>
                <li class="nav-item ml-5">
                    <a class="nav-link font-weight-bold text-uppercase" href="category">tin tức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold text-uppercase" href="category">liên hệ</a>
                </li>
                <li class="nav-item d-none d-lg-block">
                    <a class="nav-link pr-0 font-weight-bold text-uppercase shopping-card-nav" href="contact">
                        <i class="fas fa-shopping-cart"></i> 
                        <span class="notify-badge rounded-circle">9+</span>
                    </a>
                </li>
            </ul>
        </div>

    </div>
</nav>

@push('js')
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover(); 
        });
        $(function() {
            // var header = $("#navbarMain");
            // var logo = $("#sectionLogo");
            var welcome = $("#welcomeText");
            var logoScroll = $('#logoNavScroll');

            $(window).scroll(function() {    
                var scroll = $(window).scrollTop();
                if (scroll >= 60) {
                    // header.addClass("sticky");
                    // logo.addClass("d-md-none");
                    welcome.addClass("d-none");
                    logoScroll.addClass("d-block");
                } else {
                    // header.removeClass("sticky");
                    // logo.removeClass("d-md-none");
                    welcome.removeClass("d-none");
                    logoScroll.removeClass("d-block");
                }
            });
        });
    </script>
@endpush





