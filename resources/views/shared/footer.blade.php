@section('footer')

<div class="footer">
    <div class="container text-center text-lg-left">
        <div class="row py-4">
            <div class="col-12 col-lg-7">
                <div class="row footer__link mb-2">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <ul>
                            <li><b>Chính sách</b></li>
                            <li class="footer-left__detail"><a href="#">Chính sách bảo mật</a></li>
                            <li class="footer-left__detail"><a href="#">Chính sách vận chuyển</a></li>
                            <li class="footer-left__detail"><a href="#">Chính sách bảo hành</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <ul>
                            <li><b>Điều khoản</b></li>
                            <li class="footer-left__detail"><a href="#">Điều khoản sử dụng</a></li>
                            <li class="footer-left__detail"><a href="#">Điều khoản giao dịch</a></li>
                            <li class="footer-left__detail"><a href="#">Dịch vụ tiện ích</a></li>
                            <li class="footer-left__detail"><a href="#">Quyền sở hữu trí tuệ</a></li>
                        </ul>                   
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <ul>
                            <li><b>Hướng dẫn</b></li>
                            <li class="footer-left__detail"><a href="#">Hướng dẫn mua hàng</a></li>
                            <li class="footer-left__detail"><a href="#">Giao nhận và thanh toán</a></li>
                            <li class="footer-left__detail"><a href="#">Đổi trả và bảo hành</a></li>
                            <li class="footer-left__detail"><a href="#">Đăng ký thành viên</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 d-block d-lg-none">
                        <div class="footer__email">
                            <b>Gửi mail để nhận tin khuyến mãi</b>
                            <p class="email-text__main d-none d-lg-block">
                                Hãy nhập mail của bạn và gửi cho chúng tôi để nhận tin khuyến mãi
                            </p>
                            <div class="input-group mt-3">
                                <input type="email" class="form-control" placeholder="Email của bạn...">
                                <span class="input-group-btn">
                                <button class="btn" type="submit">Gửi Email</button>
                                </span>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-5 d-none d-lg-block">
                <div class="footer__email">
                    <b>Gửi mail để nhận tin khuyến mãi</b>
                    <p class="email-text__main">
                        Hãy nhập mail của bạn và gửi cho chúng tôi để nhận tin khuyến mãi
                    </p>
                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="Email của bạn...">
                        <span class="input-group-btn">
                        <button class="btn" type="submit">Gửi Email</button>
                        </span>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <div class="d-flex footer-bottom">
        <div class="container">
            <p class="footer-bottom__text" align="center">
                <b>
                © Phúc An Khang Electronic. Bản quyền thuộc về Innosoft
                </b>
            </p>
        </div>
    </div>
        
</div>
    

