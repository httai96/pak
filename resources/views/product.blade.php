@extends('layouts.web')

@section('content')

<!-- Carousel Slide (Flickity) -->

<div class="section-product-carousel">
    <div class="col-12">
        <div class="carousel" data-flickity='{ "groupCells": "100%", 
        "wrapAround": true,
        "initialIndex": 0,
        {{-- "autoPlay": true, --}}
        "freeScroll": true,
        "friction": 0.8,
        "selectedAttraction": 0.1} ' >
            @for($i=0; $i<=2; $i++)
                <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="info-left">
                        <img class="d-block w-100 product-slide-image" src="{{ asset('img/FL-EG18.png')}}" alt="First slide">
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 h-100">
                    <div class="info-right">
                        <h1>Camera 8MP IR Speed <br> Dome Hồng ngoại</h1>
                        <p> Chỉ còn: </p><h2>1.450.000đ</h2>
                        <h3 class="info-right__sale"><b >Giảm <br> 10%</b></h3>
                        <small class="info-right__small text-muted">Lorem ipsum dolor sit amet consectetur
                             adipisicing elit. 
                            Fugiat delectus consequuntur 
                            veniam inventore odio tempora explicabo!
                        </small>
                        <div class="btn-buy mt-5">
                            <button type="button" class="btn btn-primary text-uppercase btn-buy-now">Mua Ngay</button>
                        </div>
                    </div>
                </div>
            @endfor          
        </div>   
    </div>
</div>
<!-- Section Top Products -->

<div class="section-top-products mt-5">
    <div class="container">
        <div class="title-section mb-4">
            <p class="title-section__text mb-0"><b>Loại Sản Phẩm 1</b></p>
            <p class="title-section__shape mb-0">&nbsp</p>
        </div>
        
        <div class="row">
            @for($i=0; $i<=9; $i++)
            <div class="col-6 col-sm-6 col-md-3 mb-4">
                <a href="#" class="card-product align-self-stretch">
                    <div class="card card-product" style="width: 100%;">
                        <img href="#" src="{{ asset('img/UW-DS6.png')}}" alt="Card image cap" class="card-img-top">
                        <div class="card-body">
                            <a href="" class="card-title product-title"><h3>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,</h3></a>
                            <div class="card-text product-price">
                                <h3 class="card-price product-price__main">1.450.000<i>đ</i></h3>
                                <h5 class="card-price product-price__dev"><del>1.350.000</del><i>đ</i></h5>
                                <a href="" class="btn btn-info btn-add-to-cart float-right">
                                    <i class="fas fa-shopping-cart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endfor
        </div>
        <div class="row button-more justify-content-center mt-2 mb-5">
            <button type="button" class="btn btn-more">Xem Thêm</button>
        </div>
    </div>
</div>
@endsection