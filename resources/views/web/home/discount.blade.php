<div class="section-discount">
    <div class="container">
        <div class="carousel slide border border-danger" data-type="multi" data-interval="3000">
            <div class="carousel-discount-title d-flex w-100">
                <div class="title d-flex mx-auto">
                    <p class="carousel-discount-title__left">
                    </p>
                    <p class="carousel-discount-title__mid d-flex justify-content-between">
                            <i class="fas fa-circle"></i>
                        Đang giảm giá
                        <i class="fas fa-circle"></i>
                    </p>
                    <p class="carousel-discount-title__right">
                    </p>
                </div>
                
            </div>
            <!-- Flickity HTML init -->
            <div class="col-12">
                <div class="carousel my-5" data-flickity='{ "groupCells": "100%", 
                "wrapAround": true,
                "initialIndex": 0,
                "autoPlay": true,
                "freeScroll": true,
                "friction": 0.8,
                "selectedAttraction": 0.1} ' >
                    @for($i=0; $i<=11; $i++)
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex py-4 card-discount">
                        <a href="#" class="">
                            <div class="card-sale">
                                <p class="sale-shape"><b>10%</b></p>
                                <p class="sale-shape-top">&nbsp</p>
                            </div>
                            <div class="card shadow card-product h-100" style="width: 100%;">
                                <div class="image d-flex align-items-center justify-content-center">
                                    <img href="#" src="{{ asset('img/test/'.(($i%5)+1).'.png')}}" alt="Card image cap" 
                                    class="card-img-top">
                                </div>
                                {{-- </div> --}}
                                <div class="card-body">
                                    <a href="" class="card-title text-justify">
                                        {{-- , {{$i%2==0 ? "" : "jhjj jmhhkj j,hmj mjgm"}} (bo trong the h3)--}}
                                        <h4>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</h4>
                                    </a>
                                </div>
                                <div class="card-text mt-3 w-100 d-flex justify-content-between">
                                    <div>
                                        <h4 class="text-primary"><b>1.450.000<i>đ</i></b></h4>
                                        <h5><del>1.350.000<i>đ</i></del></h5>   
                                    </div>
                                    <div>
                                        <a href="" class="btn btn-add-to-cart rounded-circle">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endfor          
                </div>   
            </div>
        </div>
    </div>
</div>