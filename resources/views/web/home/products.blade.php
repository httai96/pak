<div class="section-top-products py-3">
    <div class="container">
        <div class="title-section mb-4 d-flex">
            <p class="title-section__text mb-0"><b>Loại Sản Phẩm 1</b></p>
            <div class="title-section__shape mb-0"></div>
        </div>
        <div class="row">
            @for($i=0; $i<12; $i++)
            <div class="col-md-6 col-lg-4 col-xl-3 mb-4">
                <a href="#">
                    <div class="card shadow card-product h-100" style="width: 100%;">
                        <div class="image d-flex align-items-center justify-content-center">
                            <img href="#" src="{{ asset('img/test/'.(($i%5)+1).'.png')}}" alt="Card image cap" 
                            class="card-img-top">
                        </div>
                        <div class="card-body">
                            <a href="" class="card-title text-justify">
                                {{-- , {{$i%2==0 ? "" : "jhjj jmhhkj j,hmj mjgm"}} (bo trong the h3)--}}
                                <h4>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</h4>
                            </a>
                        </div>
                        <div class="card-text mt-3 w-100 d-flex justify-content-between">
                            <div>
                                <h4 class="text-primary"><b>1.450.000<i>đ</i></b></h4>
                                <h5><del>1.350.000<i>đ</i></del></h5>   
                            </div>
                            <div>
                                <a href="" class="btn btn-add-to-cart rounded-circle">
                                    <i class="fas fa-shopping-cart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endfor
        </div>
        <div class="row justify-content-center mb-5">
            <button type="button" class="btn btn-primary">Xem Thêm</button>
        </div>
    </div>
</div>

<!-- Section Top Products 2-->
<div class="section-top-products-2 py-3">
    <div class="container">
        <div class="title-section mb-4 d-flex">
            <p class="title-section__text mb-0"><b>Loại Sản Phẩm 1</b></p>
            {{-- <div class="title-section__shape mb-0">&nbsp</div> --}}
            <div class="title-section__shape mb-0"></div>
        </div>
        
    <div class="row">
            @for($i=0; $i<12; $i++)
            <div class="col-md-6 col-lg-4 col-xl-3 mb-4">
                <a href="#">
                    <div class="card shadow card-product h-100" style="width: 100%;">
                        <div class="image d-flex align-items-center justify-content-center">
                            <img href="#" src="{{ asset('img/test/'.(($i%5)+1).'.png')}}" alt="Card image cap" 
                            class="card-img-top">
                        </div>
                        <div class="card-body">
                            <a href="" class="card-title text-justify">
                                {{-- , {{$i%2==0 ? "" : "jhjj jmhhkj j,hmj mjgm"}} (bo trong the h3)--}}
                                <h4>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</h4>
                            </a>
                        </div>
                        <div class="card-text mt-3 w-100 d-flex justify-content-between">
                            <div>
                                <h4 class="text-primary"><b>1.450.000<i>đ</i></b></h4>
                                <h5><del>1.350.000<i>đ</i></del></h5>   
                            </div>
                            <div>
                                <a href="" class="btn btn-add-to-cart rounded-circle">
                                    <i class="fas fa-shopping-cart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endfor
    </div>
        <div class="row justify-content-center mt-2 mb-5">
            <button type="button" class="btn btn-primary">Xem Thêm</button>
        </div>
    </div>
</div>

<!-- Section Top Products 3-->

<div class="section-top-products-3 py-3">
    <div class="container">
        <div class="title-section mb-4 d-flex">
            <p class="title-section__text mb-0"><b>Loại Sản Phẩm 1</b></p>
            {{-- <div class="title-section__shape mb-0">&nbsp</div> --}}
            <div class="title-section__shape mb-0"></div>
        </div>
        <div class="row">
            @for($i=0; $i<12; $i++)
            <div class="col-md-6 col-lg-4 col-xl-3 mb-4">
                <a href="#">
                    <div class="card shadow card-product h-100" style="width: 100%;">
                        <div class="image d-flex align-items-center justify-content-center">
                            <img href="#" src="{{ asset('img/test/'.(($i%5)+1).'.png')}}" alt="Card image cap" 
                            class="card-img-top">
                        </div>
                        <div class="card-body">
                            <a href="" class="card-title text-justify">
                                {{-- , {{$i%2==0 ? "" : "jhjj jmhhkj j,hmj mjgm"}} (bo trong the h3)--}}
                                <h4>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</h4>
                            </a>
                        </div>
                        <div class="card-text mt-3 w-100 d-flex justify-content-between">
                            <div>
                                <h4 class="text-primary"><b>1.450.000<i>đ</i></b></h4>
                                <h5><del>1.350.000<i>đ</i></del></h5>   
                            </div>
                            <div>
                                <a href="" class="btn btn-add-to-cart rounded-circle">
                                    <i class="fas fa-shopping-cart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endfor
        </div>
        <div class="row justify-content-center mt-2 mb-5">
            <button type="button" class="btn btn-primary">Xem Thêm</button>
        </div>
    </div>
</div>