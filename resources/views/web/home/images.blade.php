<div class="section-multi-img">
    <div class="container">
        <div class="row justify-content-center py-4">
            <div class="col-12 col-sm-6 col-lg-3" align="center">
                <div class="img-section mt-2" align="center">
                    <div class="image-news">
                        <img src="{{ asset('img/shield_bg.svg')}}" alt="shield_bg">
                    </div>
                    <h3 class="mt-3 text-center">
                        Hàng chính hãng
                    </h3>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3" align="center">
                <div class="img-section mt-2" align="center">
                    <div class="image-news">
                        <img src="{{ asset('img/support.svg')}}" alt="support">
                    </div>
                    <h3 class="mt-3 text-center">
                        Hỗ trợ lắp đặt
                    </h3>
                </div>   
            </div>
            <div class="col-12 col-sm-6  col-lg-3" align="center">
                <div class="img-section mt-2" align="center">
                    <div class="image-news">
                        <img src="{{ asset('img/bestprice_bg.svg')}}" alt="bestprice_bg">
                    </div>
                    <h3 class="mt-3 text-center">
                        Giá tốt
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>