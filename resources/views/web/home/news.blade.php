<div class="section-news my-5">
    <div class="container">
        <div class="title-section mb-4 d-flex">
            <p class="title-section__text mb-0"><b>Tin mới nhất</b></p>
            <p class="title-section__shape mb-0"></p>
        </div>
        <div class="row news">
            @for($i=0; $i<=3; $i++)
            <div class="col-12 col-sm-12 col-md-6 col-xl-3 mb-4">
                <a href="#">
                    <div class="card shadow h-100" style="width:100%;">
                        {{-- <div class="row"> --}}
                            {{-- <div class="col-5 col-sm-5 col-md-12"> --}}
                                <div class="image d-flex align-items-center justify-content-center">
                                    <img href="#" src="{{ asset('img/test/'.(($i%5)+1).'.png')}}" alt="Card image cap" 
                                    class="card-img-top">
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="col-7 col-sm-7 col-md-12"> --}}
                                <div class="card-body">
                                    <h5 class="card-text text-justify">
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                                    </h5> 
                                </div>
                                <div class="card-text text-bot text-muted text-justify">
                                    <p class="card-text ">
                                        <i class="far fa-clock"></i> 1/3/2018
                                        <i class="far fa-user ml-2"></i> Admin
                                    </p>
                                    <p class="card-text ">
                                        Ut wisi enim ad minim veniam, quis nostrud exerci 
                                        tation ullamcorper suscipit lobortis nisl ut aliquip 
                                        ex ea
                                    </p>  
                                </div>
                                {{-- </div>  --}}
                            {{-- </div> --}}
                        {{-- </div> --}}
                    </div>
                </a>    
            </div>
            @endfor
        </div>
    </div>
</div>