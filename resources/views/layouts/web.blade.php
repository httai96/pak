<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ config('app.name','INNOSOFT') }}</title>
        {{-- Style --}}
        <link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet">
        <link rel="icon" href="{!! asset('img/logo.svg') !!}"/>
        <link rel="stylesheet" href="{{ asset('css/web.css') }}?t={{ now() }}">
        @stack('css')
    </head>
    <body>
        <div id="web">
            @include('shared.navbar')

            @yield('content')

            @include('shared.footer')
        </div>
        
        <!-- Scripts -->
        {{-- <script src="{https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js}"></script> --}}
        {{-- <script src="{{ asset('jquery/dist/jquery.min.js') }}"></script> --}}
        
        <script src="{{ asset('js/web.js') }}"></script>
        @stack('js')
    </body>
</html>